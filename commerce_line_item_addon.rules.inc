<?php

/*
 * Implements hook_rules_action_info().
 */
function commerce_line_item_addon_rules_action_info() {
  $items['commerce_line_item_addon_generate'] = array(
    'label' => t('Add a line item to an order'),
    'parameter' => array(
      'order' => array(
        'label' => t('Order'),
        'type' => 'commerce_order',
      ),
      'price' => array(
        'label' => t('Price'),
        'type' => 'decimal',
      ),
      'line_item_type' => array(
        'label' => t('Line item type'),
        'options list' => 'commerce_line_item_type_options_list',
        'type' => 'text'
      ),
      'price_component' => array(
        'label' => t('Price component'),
        'options list' => 'commerce_line_item_addon_price_components_options_list',
        'type' => 'text',
      ),
      'label' => array(
        'label' => t('Line item label'),
        'type' => 'text',          
      )
    ),
    'provides' => array(
      'addon_line_item' => array(
        'label' => t('Add-on line item'),
        'type' => 'commerce_line_item',        
      )
    ),
    'group' => t('Commerce line item addon'),
  );
  
  $items['commerce_line_item_addon_delete_line_items_of_type'] = array(
    'label' => t('Delete line items of a given type from an order'),
    'parameter' => array(
      'order' => array(
        'label' => t('Order'),
        'type' => 'commerce_order',
        'wrapped' => TRUE
      ),
      'line_item_type' => array(
        'label' => t('Line item type'),
        'options list' => 'commerce_line_item_type_options_list',
        'type' => 'text'
      )
    ),
    'group' => t('Commerce line item addon'),
  );
    
  
  return $items;
}

/*
 * Implements hook_rules_condition_info().
 */
function commerce_line_item_addon_rules_condition_info() {
  $items['commerce_line_item_addon_order_has_line_item_type'] = array(
    'label' => t('Order has line item type'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('The order to check.'),
        'wrapped' => TRUE
      ),
      'line_item_type' => array(
        'label' => t('Line item type'),
        'options list' => 'commerce_line_item_type_options_list',
        'type' => 'text'
      )        
    ),
    'group' => t('Commerce line item addon'),
  );
  
  return $items;
}

/*
 * Rules action callback: add a line item of a type to an order
 */
function commerce_line_item_addon_generate($order, $amount, $line_item_type, $price_component, $label) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  
  // Create new line item
  $line_item = entity_create('commerce_line_item', array(
    'type' => $line_item_type,
    'order_id' => $order_wrapper->order_id->value(),
    'quantity' => 1,
    'label' => $label
  ));
  
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  $line_item_wrapper->commerce_unit_price->amount = $amount;
  $line_item_wrapper->commerce_unit_price->currency_code = commerce_line_item_addon_order_currency($order);
  
  // Add the price component
  $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
    $line_item_wrapper->commerce_unit_price->value(),
    $price_component,
    $line_item_wrapper->commerce_unit_price->value(),
    TRUE,
    FALSE
  );
  
  // Save the incoming line item now so we get its ID.
  commerce_line_item_save($line_item);
  
  $order_wrapper->commerce_line_items[] = $line_item;
  commerce_order_calculate_total($order);
  
  // Provide the line item back into rules
  return array('addon_line_item' => $line_item);
}

/*
 * Rules action callback: delete line items of a type
 */
function commerce_line_item_addon_delete_line_items_of_type($order_wrapper, $line_item_type) {
  commerce_line_item_addon_delete_line_items($order_wrapper, $line_item_type, TRUE);
  // Rebase order total
  commerce_order_calculate_total($order_wrapper->value());
}

/*
 * Rules condition callback: determine whether an order has line items of a type
 */
function commerce_line_item_addon_order_has_line_item_type($order_wrapper, $line_item_type) {
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    if ($line_item_wrapper->type() == $line_item_type) {
      return TRUE;
    }
  }
}

/*
 * Generates an option list from price components.
 */
function commerce_line_item_addon_price_components_options_list() {  
  $options = array();
  foreach (commerce_price_component_types() as $machine_name => $component) {
    $options[$machine_name] = $component['title'];
  }
  
  return $options;
}